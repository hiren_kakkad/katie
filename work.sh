#!/bin/sh

set -e

here=`pwd`
KATIEpath=/home/user0/repos/katie

if [ "$#" -eq 0 ]; then
   echo "==================================="
   echo " Usage: "
   echo " $ $0 <example> <directory>"
   echo "==================================="
   exit 0
fi

if [ "$#" -eq 1 ]; then
  mkdir -p $1
  cp $KATIEpath/examples/pp_to_4j/* $1
  cd $1
     ln -s $KATIEpath/run.sh
  cd $here
fi

if [ "$#" -eq 2 ]; then
  mkdir -p $2
  if [ -d "$KATIEpath/examples/$1" ] ;then
    cp $KATIEpath/examples/$1/* $2
  else
    cp $KATIEpath/$1/* $2
  fi
  cd $2
     ln -s $KATIEpath/run.sh
  cd $here
fi


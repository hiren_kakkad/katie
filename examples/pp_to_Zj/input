# Particles are encoded as follows:
# ve   ve~   e-   e+   u u~ d d~
# vmu  vmu~  mu-  mu+  c c~ s s~
# vtau vtau~ tau- tau+ t t~ b b~
# g H Z W+ W- A

# processes:
process = u  g  -> mu+ mu- u  , factor = 1
process = g  u  -> mu+ mu- u  , factor = 1
process = u~ g  -> mu+ mu- u~ , factor = 1
process = g  u~ -> mu+ mu- u~ , factor = 1
process = u  u~ -> mu+ mu- g  , factor = 1
process = u~ u  -> mu+ mu- g  , factor = 1

include file = incl_d.dat
#include file = incl_s.dat
#include file = incl_c.dat
#include file = incl_b.dat

# Number of parton flavors.
Nflavors = 5

# Interactions
switch = withQCD   Yes
switch = withQED   Yes
switch = withWeak  Yes
# pNonQCD  =  EW-couplings  Higgs-gluon-couplings  Higgs-photon-couplings
pNonQCD = 2 0 0

# Collinear pdf set. Also in TMD-case, alphaStrong is taken from there.
lhaSet = CT10nlo

# TMDlib pdf set
#TMDlibSet = MRW-CT10nlo

# TMD grid files. You can change (update) tmdTableDir between them.
tmdTableDir = /home/user0/physoft/tmdlib-1.0.26/data/BHKS/MRW-CT10nlo/
tmdpdf = g  CT10nlo_der_gluon.dat
tmdpdf = u  CT10nlo_der_u.dat
tmdpdf = u~ CT10nlo_der_ubar.dat
tmdpdf = d  CT10nlo_der_d.dat
tmdpdf = d~ CT10nlo_der_dbar.dat
tmdpdf = s  CT10nlo_der_s.dat
tmdpdf = s~ CT10nlo_der_sbar.dat
tmdpdf = c  CT10nlo_der_c.dat
tmdpdf = c~ CT10nlo_der_cbar.dat
tmdpdf = b  CT10nlo_der_b.dat
tmdpdf = b~ CT10nlo_der_bbar.dat


#Choose which initial states must be off-shell
offshell = 1 1  # eg.  g* g* -> ... 
#offshell = 0 1  # eg.  g  g* -> ...
#offshell = 1 0  # eg.  g* g  -> ...
#offshell = 0 0  # eg.  g  g  -> ...

# Center of mass energy. Must be larger than zero.
Ecm = 7000

# Phase space cuts.
# {pT|2|} is the pT of final state 2.
# {pT|2|1,2,4} is the 2nd-hardest pT of final states 1,2,4.
# {pT|2|rapidity|1,2,4} is the pT of final states 1,2,4 
#                       with the 2nd-largest rapidity.
# {pT|1+4|} is the pT of the sum of final-state momenta 1 and 4.
# {deltaR|1,2|} is the delta-R between final states 1 and 2.
# The value of  rapidity  and  pseudoRap  can be negative.
# Further available are  ET  mass  deltaPhi .
cut = {deltaR|1,3|} > 0.4
cut = {deltaR|2,3|} > 0.4
cut = {pT|1|} > 20
cut = {pT|2|} > 20
cut = {pseudoRap|1|} > -2.0
cut = {pseudoRap|2|} > -2.0
cut = {pseudoRap|1|} < 2.0
cut = {pseudoRap|2|} < 2.0
cut = {mass|1+2|} > 60
cut = {mass|1+2|} < 120
cut = {pT|3|} > 20
cut = {rapidity|3|} > -2.0
cut = {rapidity|3|} < 2.0

# Renormalization/factorization scale
scale = ({pT|1+2|}+91.1882D0+{pT|3|})/3

# Masses and widths. No width is width=0
mass = Z   91.1882  2.4952
mass = W   80.419   2.21
mass = H  125.0     0.00429
mass = t  173.5
# Interactions
switch = withHiggs No
switch = withHG    No
# Couplings. You can set either alphaEW or Gfermi
coupling = Gfermi 1.16639d-5

# Number of nonzero-weight phase space points to be spent on optimization.
Noptim = 100,000

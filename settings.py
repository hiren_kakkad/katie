
# Path to the AVHLIB directory
AVHLIBpath = '/home/user0/repos/avhlib'
#AVHLIBpath = '/home/user0/old/repos/avhlib'

# Path to the directory where libLHAPDF.so is
LHAPDFpath = '/usr/lib'

# Fortran compiler with flags
FC = 'gfortran -fcheck=bounds'

# The following is only necessary if you want to use TMDlib:
#
# Path to the directory where libTMDlib.so is
TMDLIBpath = '/home/user0/physoft/installs/lib'
#TMDLIBpath = '/home/user0/physoft/tmdlib-1.0.29/lib'
#TMDLIBpath = '/home/user0/physoft/tmdlib-2.0.2/lib'
#
# Do you want to use multiple TMD sets (available from version 2.0.2)?
#TMDLIBmulti = 'yes'
#
# Path to the directory where libgsl.so is
GSLpath = '/usr/lib'

# No need to change the following if you want to keep the defaults:
#
# Default: 'dynamic'
#LINKMETHOD = 'static'
#
# The following only concerns dynamic linking.
# Default: 'gcc' for MacOS, and 'ld' for other platforms
#LINKER = 'gcc'

# If you want to combine KaTie with Mincas, put the path to
# the directory with the "src" and "include" of Mincas, and
# the directory with the source files of Merger here:
#MINCASpath = '/home/user0/physoft/MINCAS'
#MERGERpath = '/tmp/Merger'


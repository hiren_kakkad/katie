module katie_pdfs
  use avh_iounits
  use avh_lagrangian
  use avh_trivinp
  use avh_mathcnst
  use katie_partlumi ,only: set_Nflavors
  implicit none
  private
  public :: pdfs_init,set_tmdpath,add_tmdpdf,pdfMesh
  public :: pdfvec_lhapdf,pdfvec_grid
  public :: pdfunc_lhapdf,pdfunc_grid
  public :: pdfs_init_tmdlib,pdfvec_tmdlib,pdfunc_tmdlib !{TMDlibVersion!=000!}
  public :: pdfvec_tmdlibset,pdfunc_tmdlibset !{TMDlibVersion=2xx!}
  public :: alphasFunc

  type(trivinp_type),save,protected :: pdfMesh( -6 -2*13 : 6 -1*13 )
  integer,save :: lgn2pdf(nulParticle:maxParticle)

  integer,parameter :: gl=0,uQ=1,dQ=2,sQ=3,cQ=4,bQ=5,tQ=6
  integer,parameter :: pdf2lha(-6:6)=[-6,-5,-4,-3,-1,-2,0,2,1,3,4,5,6]

  integer,save :: tmdFlavor(13),tmdNflavors=0

  character(256),save :: path

!{TMDlibVersion=1,2
  interface pdfs_init_tmdlib
    module procedure init_tmdlib,init_tmdlib_nr
  end interface
!}TMDlibVersion

contains


  subroutine pdfs_init( Nflavors ,lhaSet )
  integer,intent(in) :: Nflavors
  character(*),intent(in) :: lhaSet
  call set_Nflavors( Nflavors )
  lgn2pdf = gl
  lgn2pdf(uQuark)=uQ; lgn2pdf(-uQuark)=-lgn2pdf(uQuark);
  lgn2pdf(dQuark)=dQ; lgn2pdf(-dQuark)=-lgn2pdf(dQuark);
  lgn2pdf(cQuark)=cQ; lgn2pdf(-cQuark)=-lgn2pdf(cQuark);
  lgn2pdf(sQuark)=sQ; lgn2pdf(-sQuark)=-lgn2pdf(sQuark);
  lgn2pdf(tQuark)=tQ; lgn2pdf(-tQuark)=-lgn2pdf(tQuark);
  lgn2pdf(bQuark)=bQ; lgn2pdf(-bQuark)=-lgn2pdf(bQuark);
  call InitPDFsetByName(trim(lhaSet)//'.LHgrid')
  tmdNflavors = 0
  end subroutine

  subroutine set_tmdpath( pathVal )
  character(*),intent(in) :: pathVal
  path = pathVal
  end subroutine

  subroutine add_tmdpdf( iBeam ,iParton ,fileName )
  integer,intent(in) :: iBeam ,iParton
  character(*),intent(in) :: fileName
  if (all(tmdFlavor(1:tmdNflavors).ne.lgn2pdf(iParton))) then
    tmdNflavors = tmdNflavors+1
    tmdFlavor(tmdNflavors) = lgn2pdf(iParton)
  endif
  call pdfMesh(lgn2pdf(iParton)+13*iBeam)%read_file(trim(path)//trim(fileName))
  end subroutine

  function alphasFunc(xx) result(rslt)
  !(realknd2!),intent(in) :: xx
  !(realknd2!) :: rslt,alphasPDF
  rslt = alphasPDF(xx)
  end function
  
  function pdfvec_lhapdf( xx ,qq ) result(list)
  intent(in) :: xx,qq
  !(realknd2!) :: xx,qq,list(-6:6)
  list = 0
  call evolvePDF(xx,qq,list)
  list = list(pdf2lha)/xx
  end function

  function pdfunc_lhapdf( iParton ,xx ,qq ) result(rslt)
  intent(in) :: iParton,xx,qq
  integer :: iParton
  !(realknd2!) :: xx,qq,rslt,list(-6:6)
  rslt = 0
  call evolvePDF(xx,qq,list)
  rslt = list(pdf2lha(lgn2pdf(iParton)))/xx
  end function


  function pdfvec_grid( iBeam ,xx ,qq ,kTsq ) result(list)
  intent(in) :: iBeam ,xx,kTsq,qq
  !(realknd2!) :: xx,kTsq,qq,list(-6:6),logx,logkTsq,logQsq,Qsq
  integer :: iBeam,ii
  list = 0
  select case (pdfMesh(tmdFlavor(1)+13*iBeam)%Ndim)
  case default
    logx = log(xx)
    logkTsq = log(kTsq)
    logQsq = log(qq*qq)
    do ii=1,tmdNflavors
      list(tmdFlavor(ii)) = pdfMesh(tmdFlavor(ii)+13*iBeam)%evaluate( logx ,logkTsq ,logQsq )
      list(tmdFlavor(ii)) = list(tmdFlavor(ii))/xx
    enddo
  case (2)
    logx = log(xx)
    logkTsq = log(kTsq)
    do ii=1,tmdNflavors
      list(tmdFlavor(ii)) = pdfMesh(tmdFlavor(ii)+13*iBeam)%evaluate( logx ,logkTsq )
      list(tmdFlavor(ii)) = list(tmdFlavor(ii))/xx
    enddo
  end select
  end function

  function pdfunc_grid( iBeam ,iParton ,xx ,qq ,kTsq ) result(rslt)
  intent(in) :: iBeam,iParton,xx,qq,kTsq
  integer :: iBeam,iParton
  !(realknd2!) :: xx,kTsq,qq,rslt,logx,logkTsq,logQsq,Qsq
  rslt = 0
  select case (pdfMesh(lgn2pdf(iParton)+13*iBeam)%Ndim)
  case default
    logx = log(xx)
    logkTsq = log(kTsq)
    logQsq = log(qq*qq)
    rslt = pdfMesh(lgn2pdf(iParton)+13*iBeam)%evaluate( logx ,logkTsq ,logQsq )
    rslt = rslt/xx
  case (2)
    logx = log(xx)
    logkTsq = log(kTsq)
    rslt = pdfMesh(lgn2pdf(iParton)+13*iBeam)%evaluate( logx ,logkTsq )
    rslt = rslt/xx
  end select
  end function


!{TMDlibVersion=1,2
  subroutine init_tmdlib( tmdSet )
  character(*),intent(in) :: tmdSet
  integer :: TMDnumberPDF
  call TMDinit(TMDnumberPDF(trim(adjustl(tmdSet))//char(0)))
  end subroutine

  subroutine init_tmdlib_nr( tmdSet )
  integer,intent(in) :: tmdSet
  call TMDinit(tmdSet)
  end subroutine

  function pdfvec_tmdlib( xx ,qq ,kTsq ,kf ) result(list)
  intent(in) :: kf,xx,kTsq,qq
  integer :: kf
  !(realknd2!) :: xx,xbar,kTsq,qq,list(-6:6),hh(2)
  list = 0
  xbar = 0
  call TMDpdf( kf ,xx ,xbar ,sqrt(kTsq) ,qq &
      ,list(uQ),list(-uQ) ,list(dQ),list(-dQ) ,list(sQ),list(-sQ) &
      ,list(cQ),list(-cQ) ,list(bQ),list(-bQ) ,list(gl) )
!  if (kf.gt.0) then
!    list(1) = list(1) + list(-1)
!    list(2) = list(2) + list(-2)
!  else
!    hh(1) = list(-1)
!    hh(2) = list(-2)
!    list(-1) = list(1) + list(-1)
!    list(-2) = list(2) + list(-2)
!    list(1:2) = hh(1:2)
!  endif
!  list(-3) = list(3)
!  list(-4) = list(4)
!  list(-5) = list(5)
  list = list/xx
  end function

  function pdfunc_tmdlib( iParton ,xx ,qq ,kTsq ,kf ) result(rslt)
  intent(in) :: iParton,kf,xx,kTsq,qq
  integer :: iParton,kf
  !(realknd2!) :: xx,kTsq,qq,list(-6:6),hh,rslt
  list = pdfvec_tmdlib( xx ,qq ,kTsq ,kf )
  rslt = list(lgn2pdf(iParton))
  end function
!}TMDlibVersion

!{TMDlibVersion=2
  function pdfvec_tmdlibset( xx ,qq ,kTsq ,kf ,iSet ) result(list)
  intent(in) :: iSet,kf,xx,kTsq,qq
  integer :: kf,iSet
  !(realknd2!) :: xx,xbar,kTsq,qq,list(-6:6),hh(2)
  list = 0
  xbar = 0
  call TMDpdfset( iSet ,kf ,xx ,xbar ,sqrt(kTsq) ,qq &
      ,list(uQ),list(-uQ) ,list(dQ),list(-dQ) ,list(sQ),list(-sQ) &
      ,list(cQ),list(-cQ) ,list(bQ),list(-bQ) ,list(gl) )
  list = list/xx
  end function

  function pdfunc_tmdlibset( iParton ,xx ,qq ,kTsq ,kf ,iSet ) result(rslt)
  intent(in) :: iSet,iParton,kf,xx,kTsq,qq
  integer :: iParton,kf,iSet
  !(realknd2!) :: xx,kTsq,qq,list(-6:6),hh,rslt
  list = pdfvec_tmdlibset( xx ,qq ,kTsq ,kf ,iSet )
  rslt = list(lgn2pdf(iParton))
  end function
!}TMDlibVersion

end module


